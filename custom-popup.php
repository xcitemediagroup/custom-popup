<?php

/**
 * Plugin Name: Custom Popup
 * Plugin URI: http://www.wordpress.com
 * Description: Custom popup
 * Version: 1.0
 * Author: Webstantly
 * Author URI:  Webstantly
 * License: GPL2 license
 */
add_action('wp_enqueue_scripts', 'myplugin_scripts');
function myplugin_scripts() {
    wp_register_style('style', plugin_dir_url(__FILE__) . 'css/style.css');
    wp_register_style('fancybox', plugin_dir_url(__FILE__) . 'fancybox/jquery.fancybox.css');
    wp_register_script('fancybox', plugin_dir_url(__FILE__) . 'fancybox/jquery.fancybox.js');
    wp_register_script('fancybox.pack', plugin_dir_url(__FILE__) . 'fancybox/jquery.fancybox.pack.js');
    wp_register_script('script', plugin_dir_url(__FILE__) . 'js/script.js');

    wp_enqueue_style('style');
    wp_enqueue_style('fancybox');
    wp_enqueue_script('fancybox', array('jquery'));
    wp_enqueue_script('fancybox.pack', array('jquery'));
    wp_enqueue_script('script', array('jquery'));
}
add_shortcode( 'show_popup', 'show_popup_func' );
function show_popup_func(){
    $content = carbon_get_post_meta( get_the_ID(), 'crb_content' );
    $background = carbon_get_post_meta( get_the_ID(), 'crb_background' );
    $visible = carbon_get_post_meta( get_the_ID(), 'crb_visibility' );
    $image = wp_get_attachment_image_src($background, 'large');
    if($visible) :
        ?>
        <a id="inline-popup" class="fancybox" href="#adspopup" style="position: absolute; z-index: -9;"></a>
        <div style="display: none;">
            <div id="adspopup" class="homepopup" style="background-image: url('<?php echo $image[0]; ?>')">
                <div class="padder">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <?php
    endif;
}